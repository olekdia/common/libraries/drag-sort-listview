object Versions {
    const val kotlin = "1.7.20"
    const val junit = "4.13"
    const val robolectric = "4.5.1"
    const val mockk = "1.10.0"

    object olekdia {
        const val common = "0.6.1"
        const val common_android = "3.7.4"
    }

    object sdk {
        const val min = 21
        const val target = 32
        const val compile = 32
    }
    const val buildTools = "33.0.0"

    object androidx {
        const val annotations = "1.5.0"
        const val appcompat = "1.5.1"
        const val material = "1.7.0"
        const val core = "1.8.0"

        const val test_core = "1.4.0"
        const val test_runner = "1.4.0"
        const val test_rules = "1.4.0"
    }

    const val android_gradle = "7.3.1"
}


object Libs {
    val junit = "junit:junit:${Versions.junit}"
    val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"
    val mockk_jvm = "io.mockk:mockk:${Versions.mockk}"

    object kotlin {
        val reflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"
    }
    object olekdia {
        private val common_prefix = "com.olekdia:multiplatform-common"
        val common = "$common_prefix:${Versions.olekdia.common}"
        val common_jvm = "$common_prefix-jvm:${Versions.olekdia.common}"
        val common_js = "$common_prefix-js:${Versions.olekdia.common}"
        val common_native = "$common_prefix-native:${Versions.olekdia.common}"
        val common_android = "com.olekdia:android-common:${Versions.olekdia.common_android}"
    }

    object androidx {
        val annotations = "androidx.annotation:annotation:${Versions.androidx.annotations}"
        val appcompat = "androidx.appcompat:appcompat:${Versions.androidx.appcompat}"
        val material = "com.google.android.material:material:${Versions.androidx.material}"
        val core = "androidx.core:core:${Versions.androidx.core}"

        val test_core = "androidx.test:core:${Versions.androidx.test_core}"
        val test_runner = "androidx.test:runner:${Versions.androidx.test_runner}"
        val test_rules = "androidx.test:rules:${Versions.androidx.test_rules}"
    }

    object plugin {
        val kotlin_gradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
        val android_gradle = "com.android.tools.build:gradle:${Versions.android_gradle}"
    }
}
