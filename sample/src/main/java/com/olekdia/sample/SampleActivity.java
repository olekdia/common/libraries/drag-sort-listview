package com.olekdia.sample;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.olekdia.dslv.DragSortListView;

import androidx.appcompat.app.AppCompatActivity;

public class SampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        final DragSortListView dragList = (DragSortListView) findViewById(R.id.drag_list);
        final DragListAdapter listAdapter = new DragListAdapter((FrameLayout) findViewById(R.id.content_container),
                                                                    dragList);
    }
}
