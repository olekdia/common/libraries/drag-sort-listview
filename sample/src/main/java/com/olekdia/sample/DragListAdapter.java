package com.olekdia.sample;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.olekdia.dslv.DragSortController;
import com.olekdia.dslv.DragSortListView;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.ColorInt;

public class DragListAdapter extends BaseAdapter implements
        DragSortListView.DropListener, DragSortListView.RemoveListener, DragSortListView.SelectListener,
        AdapterView.OnItemClickListener {

    @ColorInt
    public static final int DRAG_FADED_COLOR = 0x50956092;
    @ColorInt
    public static final int REMOVE_FADED_COLOR = 0x55A1406D;

    private final FrameLayout mContainer;
    private final DragSortListView mDragList;
    private final LayoutInflater mInflater;
    private final DragSortController mDragController;

    private ArrayList<String> mDataProvider = new ArrayList<>(
            Arrays.asList("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"));

    private Resources mRes;

    public DragListAdapter(final FrameLayout container, final DragSortListView dragList) {
        mContainer = container;
        mDragList = dragList;
        mInflater = LayoutInflater.from(dragList.getContext());
        mRes = dragList.getContext().getResources();

        mDragController = new DragController(dragList, this);
        mDragController.setFlingLeftEnabled(true);
        mDragController.setFlingRightEnabled(true);
        dragList.setFloatViewManager(mDragController);
        dragList.setOnTouchListener(mDragController);
        dragList.setDropListener(this);
        dragList.setRemoveListener(this);
        dragList.setSelectListener(this);
        dragList.setOnItemClickListener(this);

        dragList.setAdapter(this);
    }

    @Override
    public int getCount() {
        return mDataProvider.size();
    }

    @Override
    public String getItem(int position) {
        return mDataProvider.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView field;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, parent, false);
        }

        field = (TextView) convertView;

        field.setText(getItem(position));
        field.setBackgroundColor(mRes.getColor(position % 2 == 0 ? R.color.secondary_light : R.color.primary_light));
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }


    @Override
    public void drop(int from, int to, View floatView) {
        final String item = mDataProvider.remove(from);
        mDataProvider.add(to, item);
        notifyDataSetChanged();
    }

    @Override
    public void remove(int position) {
        mDataProvider.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public void select(int position) {
        Toast.makeText(mContainer.getContext(), "Selected " + position, Toast.LENGTH_SHORT).show();
    }

    public class DragController extends DragSortController {

        private DragListAdapter mAdapter;
        private DragSortListView mDragList;


        public DragController(final DragSortListView dragList, final DragListAdapter adapter) {
            super(dragList, 0, DragSortController.ON_LONG_PRESS, FLING_REMOVE);
            setSelectEnabled(true);
            mDragList = dragList;
            mAdapter = adapter;
        }

        @Override
        public View onCreateFloatView(int position) {
            final View v = mAdapter.getView(position, null, mDragList);

            if (isFlinging()) {
                v.setBackgroundColor(REMOVE_FADED_COLOR);
            } else {
                v.setBackgroundColor(DRAG_FADED_COLOR);
            }

            return v;
        }

        @Override
        public void onDestroyFloatView(View floatView) {
        }
    }
}